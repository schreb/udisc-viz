import os
from pathlib import Path

class Config:
    BASE_FOLDER = Path(__file__).parent
    INPUT_FOLDER = BASE_FOLDER / 'input'
    OUTPUT_FOLDER = BASE_FOLDER / 'output'