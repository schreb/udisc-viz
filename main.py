import argparse
import logging
from pathlib import Path

logger = logging.getLogger(__name__)

# local imports
from lib import load, visualize, create_summary


def parse_args():
    parser = argparse.ArgumentParser('UDisc Data Cleaner')
    parser.add_argument('-i', '--input_file',
                        help='File path to raw extract csv from UDisc, should end in ".csv"')
    parser.add_argument('-s', '--save_file',
                        help='File path to save clean dataset, should end in ".csv"')
    parser.add_argument('-n', '--player_name', default=None,
                        help='(Optional) Name of the player, '
                             'if None, will deduce the name based on the most frequently used name')
    parser.add_argument('-d', '--display_name', default=None,
                        help='Name to put in the middle of the viz')

    # TODO: add 1) clean name for middle of viz and 2) a `produce visualization` param to save visualization
    parser.add_argument('-v', '--viz', action='store_true',
                        help='If provided, after cleaning the data add the visualization')

    args = parser.parse_args()

    return args


def setup_logging():
    logging.basicConfig(
        level=logging.INFO,
        format='[%(asctime)s] %(levelname)s %(module)s:%(lineno)d (%(funcName)s) - %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )


if __name__ == '__main__':
    setup_logging()
    args = parse_args()
    data = load(input_file=args.input_file,
                save_file=args.save_file,
                player_name=args.player_name)

    if args.viz is True:
        visualize(data=data,
                  player_name=args.display_name)
