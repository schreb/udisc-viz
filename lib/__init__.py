from pathlib import Path
import logging

logger = logging.getLogger(__name__)

try:
    from config import Config
except ImportError:
    from .config import Config

from .clean_data import DataCleaner
from .visualizations import create_visualizations
from .summarize_data import create_summary


def load(input_file: [Path, str], save_file: [Path, str], player_name: str = None):
    """Load and save the data

    :param input_file: str or pathlib.Path
       File path to raw extract csv from UDisc, should end in ".csv"
    :param save_file:  str or pathlib.Path
       File path to save clean dataset, should end in ".csv"
    :param player_name:  str, default None
        Name of the player, if None, will deduce the name based on the most frequently used name
    :return: pd.DataFrame with clean data

    """

    cleaner = DataCleaner()
    return cleaner.load(input_file=input_file, save_file=save_file, player_name=player_name)


def visualize(data, player_name: str = None):
    summary = create_summary(data=data)
    create_visualizations(data, summary, player_name)

    return True
