import os
from pathlib import Path
import pandas as pd
import logging

logger = logging.getLogger(__name__)


class DataCleaner:
    def __init__(self):
        self.select_hole_names = ['Hole{:d}'.format(h) for h in range(1, 19)]
        self.index_cols = ['CourseName', 'LayoutName', 'Date']

    def load_data(self, file_path: [str, Path]) -> pd.DataFrame:
        """Read the data, which should be in the raw export format from UDisc

        :param file_path: str or pathlib.Path
            the path to the location the raw data file is saved
        :return: pd.DataFrame
            The loaded data frame with the data
        """
        logger.info(f'Loading raw data...')
        raw_scores = pd.read_csv(file_path, parse_dates=['Date'])
        logger.info(f'Raw data loaded, found {raw_scores.shape[0]} rows')

        return raw_scores

    def filter_data(self, raw_scores: pd.DataFrame) -> pd.DataFrame:
        """Removes rows that are incompatible, mainly lines that are note from
        completed 18 hole courses

        :param raw_scores:
        :return:
        """
        logger.info('Cleaning and formatting data')

        # filter for only 18 hole courses
        all_hole_names = [c for c in raw_scores.columns if c.find('Hole') > -1]

        # drop any records that have not poplated 1-18
        raw_scores = raw_scores.dropna(axis=0, how='any', subset=self.select_hole_names)

        # drop items where there are zeros in holes
        mask = ~((raw_scores.loc[:, self.select_hole_names] == 0).sum(axis=1) > 0)
        raw_scores = raw_scores.loc[mask, :]

        # filter out items with extra holes as well
        extra_holes = (set(all_hole_names).difference(set(self.select_hole_names)))

        mask = ((~raw_scores.loc[:, extra_holes].isnull()).sum(axis=1) > 0)
        raw_scores = raw_scores.loc[~mask, :]

        return raw_scores

    def clean_data(self, raw_scores: pd.DataFrame) -> pd.DataFrame:
        """Remove par values as rows, and add in as a column mapped to the hole

        :param raw_scores: pd.DataFrame
            post-cleaning dataset
        :return: pd.DataFrame

        """

        logger.info('Enriching and cleaning data')

        # melt down the data into a one-line-per-hold format
        raw_scores = (raw_scores.loc[:, self.index_cols + ['PlayerName'] + self.select_hole_names]
                      .melt(id_vars=self.index_cols + ['PlayerName'],
                            var_name='Hole',
                            value_name='Score')
                      )

        # remove the par values into their own frame
        mask = (raw_scores['PlayerName'] == 'Par')
        pars = raw_scores.loc[mask, :].drop_duplicates()
        raw_scores = raw_scores.loc[~mask, :]

        # merge par values as a separate column
        starting_rows = raw_scores.shape[0]

        total_scores = (pd.merge(left=raw_scores,
                                 right=pars,
                                 how='left',
                                 left_on=self.index_cols + ['Hole'],
                                 right_on=self.index_cols + ['Hole'],
                                 suffixes=['', '_par']
                                 )
                        .drop(['PlayerName_par'], axis=1)
                        .rename(columns={'Score_par': 'Par'})
                        )

        assert total_scores.shape[0] == starting_rows, 'Merge Error: additional rows added'

        # additional data enrichment

        # convert Hole column to an integer and sort by date/hole
        total_scores['Hole'] = total_scores['Hole'].str.replace('Hole', '').astype(int)
        total_scores = total_scores.sort_values(['Date', 'Hole'])
        # create a unique index concatenation
        # TODO: this this column needed? Should be redundant with the much cleaner "Round_Num" column
        total_scores['unique_index'] = total_scores.loc[:, self.index_cols].astype(str).sum(axis=1)
        # increasing integer index for each of the rounds
        total_scores['Round_Num'] = total_scores.groupby('Date').ngroup()

        for col in ['PlayerName', 'CourseName', 'LayoutName']:
            total_scores[col] = total_scores[col].str.strip()

        return total_scores

    @staticmethod
    def add_active_player_index(total_scores: pd.DataFrame, player_name: str) -> pd.DataFrame:
        # add active player distinction
        total_scores['is_active'] = False
        mask = (total_scores['PlayerName'] == player_name)
        total_scores.loc[mask, 'is_active'] = True

        return total_scores

    @staticmethod
    def deduce_player_name(raw_scores: pd.DataFrame) -> str:
        active_name = raw_scores['PlayerName'].value_counts().nlargest(1).index[0]

        return active_name

    def remove_blank_rounds(self, data):
        # remove rounds where no played is "Active"
        # which can occur when names are mad up for that round (e.g. "ben and danny")
        round_num_map = data.groupby('Round_Num', as_index=False)['is_active'].sum()
        mask = (round_num_map['is_active'] == 0)
        bad_rounds = round_num_map.loc[mask, 'Round_Num'].unique().tolist()
        mask = (~data['Round_Num'].isin(bad_rounds))
        data = data.loc[mask, :]

        # re-add round num now that all bad data is gone
        data['Round_Num'] = data.groupby('Date').ngroup()

        return data

    def load(self, input_file: [Path, str], save_file: [Path, str] = None, player_name: str = None) -> pd.DataFrame:
        """Return clean dataset, and save to designated splt

        :param input_file: str or pathlib.Path
            File path to raw extract csv from UDisc, should end in ".csv"
        :param save_file:  str or pathlib.Path, default = None
            File path to save clean dataset, should end in ".csv". If None, will not save
        :param player_name:  str, default None
            Name of the player, if None, will deduce the name based on the most frequently used name
        :return: pd.DataFrame with clean data
        """

        raw_scores = self.load_data(input_file)

        # filter the data
        raw_scores = self.filter_data(raw_scores=raw_scores)

        # enrich and clean data
        raw_scores = self.clean_data(raw_scores=raw_scores)

        # add in a column for the active player name
        if player_name is None:
            player_name = self.deduce_player_name(raw_scores=raw_scores)
            logging.info(f'No player name provided, deduced name is {player_name}')

        raw_scores = self.add_active_player_index(total_scores=raw_scores, player_name=player_name)

        raw_scores = self.remove_blank_rounds(raw_scores)
        raw_scores['Under_Over'] = raw_scores['Score'].sub(raw_scores['Par'])

        if save_file is not None:
            raw_scores.to_csv(save_file, index=False)



        logger.info(f'Clean data file saved to {save_file}')
        return raw_scores
