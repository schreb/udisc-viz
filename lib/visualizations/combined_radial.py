import plotly.graph_objs as go
import logging
from .. import Config

logger = logging.getLogger(__name__)

MAX_SIZE = 50
# large-to small size by ring
SIZE_INCREMENT = (25 - 2) / 18


def calculate_positioning(data, summary):
    num_rounds = data['Round_Num'].max() + 1
    increment = 360 / num_rounds
    # radians = np.radians

    data['degrees'] = data['Round_Num'].mul(increment)
    summary['degrees'] = summary.index.get_level_values('Round_Num').values * increment

    # rings should be concentric
    data['r_val'] = data['Hole'] * (MAX_SIZE / 18)
    data['markersize'] = (data['Hole'] * SIZE_INCREMENT) + 2

    return data, summary


def get_detail_text(srs):
    if srs['Under_Over'] > 0:
        score = f"+{srs['Under_Over']:.0f}"
    else:
        score = f"{srs['Under_Over']:.0f}"

    txt = (f"<b>Course:</b>{srs['CourseName']}<br>"
           f"<b>Hole:</b>{srs['Hole']}<br>"
           f"<b>Date:</b>: {srs['Date']:%-d-%b-%Y}<br>"
           f"<b>Score:</b> {srs['Score']:.0f} (Par {srs['Par']:.0f})<br>"
           )

    return txt


def get_color(under_over):
    if under_over >= 2:
        return "#41bbc5"
    elif under_over > 0:
        return "#90ea66"
    elif under_over == 0:
        return '#298837'
    else:
        return "#a8c280"


def create_scatter_plot(plot_data):
    plot = go.Scatterpolar(r=plot_data['r_val'],
                           theta=plot_data['degrees'],
                           mode='markers',
                           fillcolor=None,
                           marker=go.scatterpolar.Marker(size=plot_data['markersize'].values,
                                                         color=[get_color(x) for x in plot_data['Under_Over']],
                                                         opacity=0.5),
                           hovertext=plot_data['txt'].values,
                           hoverinfo='text',
                           name='holes'
                           )

    return plot


def create_polar_bar(summary):
    plot = go.Barpolar(r=summary['Under_Over'].values,
                       theta=summary['degrees'].values,
                       base=MAX_SIZE + 2,
                       marker_color='blue',
                       marker_line_color='white',
                       marker_line_width=2,
                       hovertext=summary['txt'].values,
                       hoverinfo='text',
                       opacity=0.8,
                       name='rounds'
                       )
    return plot


def create_plot(data, summary, input_name=None):
    data, summary = calculate_positioning(data, summary)

    data['txt'] = data.apply(get_detail_text, axis=1)

    mask = (data['is_active'] == True)
    plot_data = data.loc[mask, :]

    plots = [
        create_polar_bar(summary),
        create_scatter_plot(plot_data)
    ]

    layout = go.Layout(polar=go.layout.Polar(
        radialaxis=go.layout.polar.RadialAxis(range=[0, MAX_SIZE + summary['Under_Over'].max() + 5],
                                              showticklabels=False, ticks='', showline=False,
                                              gridcolor=None,
                                              showgrid=False,

                                              ),
        angularaxis=go.layout.polar.AngularAxis(showticklabels=False,
                                                ticks='',
                                                showgrid=False,
                                                gridcolor=None,
                                                ),
        bgcolor='grey'
    ),
    )

    fig = go.Figure(data=plots, layout=layout)

    # TODO: make sure weird characters in name do not cause issues
    save_loc = Config.OUTPUT_FOLDER / f'detail_radial_{input_name}.html'
    with open(save_loc, 'w') as f:
        f.write(fig.to_html())
    logger.info(f'Saved summary radial plot to {save_loc}')

    return fig
