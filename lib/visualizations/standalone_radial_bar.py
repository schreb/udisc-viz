import pandas as pd
import plotly.graph_objs as go
import plotly.offline as pyo
import logging

logger = logging.getLogger(__name__)

from lib import Config


def create_summary_radial_plot(summary: pd.DataFrame, input_name=None):
    color_option = ["#41bbc5", "#2d595a", "#90ea66", "#298837", "#a8c280"]

    unique_courses = summary.index.get_level_values('CourseName').unique().tolist()

    colors = [color_option[unique_courses.index(c) % len(color_option)]
              for c in summary.index.get_level_values('CourseName')]

    plots = [go.Barpolar(r=summary['Under_Over'].values,
                         #             theta=
                         base=20,
                         width=4,
                         marker_color=colors,
                         marker_line_color='white',
                         marker_line_width=2,
                         hovertext=summary['txt'].values,
                         hoverinfo='text',
                         opacity=0.8,
                         )
             ]

    if input_name is not None:
        display_name = '<br>'.join(input_name.split(' '))
    else:
        display_name = ''

    annotations = [go.layout.Annotation(text=display_name, ax=0, ay=0, font=dict(size=15, color='White'))
                   ]

    layout = go.Layout(polar=go.layout.Polar(
        radialaxis=go.layout.polar.RadialAxis(range=[0, summary['Under_Over'].max() + 25],
                                              showticklabels=False, ticks='', showline=False,
                                              gridcolor=None,
                                              showgrid=False,

                                              ),
        angularaxis=go.layout.polar.AngularAxis(showticklabels=False,
                                                ticks='',
                                                showgrid=False,
                                                gridcolor=None,
                                                ),
        bgcolor='black'
    ),
        annotations=annotations,
    )

    fig = go.Figure(data=plots, layout=layout)

    # pyo.iplot(fig)
    # TODO: make sure weird characters in name do not cause issues
    save_loc = Config.OUTPUT_FOLDER / f'radial_summary_{input_name}.html'
    with open(save_loc, 'w') as f:
        f.write(fig.to_html())
    logger.info(f'Saved summary radial plot to {save_loc}')
    return fig
