from .combined_radial import create_plot
from .standalone_radial_bar import create_summary_radial_plot


def create_visualizations(data, summary, input_name=None):
    create_plot(data, summary, input_name)
    create_summary_radial_plot(summary, input_name)
