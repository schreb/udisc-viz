import pandas as pd


def summarize_by_course(data: pd.DataFrame) -> pd.DataFrame:
    mask = (data['is_active'] == True)
    summary = (data.loc[mask, :]
               .groupby(['CourseName', 'Round_Num'])[['Score', 'Par']].sum()
               )

    summary['Under_Over'] = summary['Score'].sub(summary['Par'])

    return summary


def get_detail_map(data: pd.DataFrame) -> pd.DataFrame:
    detail_map = (data
                  .groupby(['Round_Num', 'CourseName', 'Date', 'LayoutName'])['PlayerName'].agg(
        lambda srs: ','.join(srs.unique()))
                  .reset_index()
                  .set_index('Round_Num')
                  .to_dict(orient='Index')
                  )

    return detail_map


def get_text(srs, detail_map):
    course, id_ = srs.name

    details = detail_map[id_]

    if srs['Under_Over'] > 0:
        score = f"+{srs['Under_Over']:.0f}"
    else:
        score = f"{srs['Under_Over']:.0f}"

    txt = (f"<b>Course:</b>{course}<br>"
           f"<b>Date:</b>: {details['Date']:%-d-%b-%Y}<br>"
           f"<b>Score:</b> {score}<br>"
           f"<b>Players</b>: {details['PlayerName']}"
           )

    return txt


def create_summary(data: pd.DataFrame) -> pd.DataFrame:
    summary = summarize_by_course(data)
    detail_map = get_detail_map(data)

    summary['txt'] = summary.apply(get_text, detail_map=detail_map,
                                   axis=1)

    return summary
